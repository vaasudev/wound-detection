import tensorflow as tf

from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Conv2D, Conv2DTranspose, BatchNormalization, Activation, UpSampling2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import concatenate
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import backend as K

import wandb
from wandb.keras import WandbCallback

import os

import parameters as param
from .custom_metrics import dice_coef
from .custom_losses import dice_coef_loss

wandb.init(project="wound-segmentation")


def build_model1():
    strategy = tf.distribute.MirroredStrategy()
    with strategy.scope():
        inputs = Input((param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS))
        # s = Lambda(lambda x: x / 255.)(inputs)

        # c1 = Conv2D(64, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(s)
        c1 = Conv2D(32, (3, 3), activation='relu', padding='same')(inputs)
        # c1 = Dropout(0.1)(c1)
        c1 = Conv2D(64, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c1)
        p1 = MaxPooling2D((2, 2))(c1)

        c2 = Conv2D(128, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(p1)
        # c2 = Dropout(0.1)(c2)
        c2 = Conv2D(128, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c2)
        p2 = MaxPooling2D((2, 2))(c2)

        c3 = Conv2D(256, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(p2)
        # c3 = Dropout(0.2)(c3)
        c3 = Conv2D(256, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c3)
        p3 = MaxPooling2D((2, 2))(c3)

        c4 = Conv2D(512, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(p3)
        c4 = Dropout(0.5)(c4)
        c4 = Conv2D(512, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c4)
        p4 = MaxPooling2D(pool_size=(2, 2))(c4)

        c5 = Conv2D(1024, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(p4)
        c5 = Conv2D(1024, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c5)
        c5 = Dropout(0.5)(c5)

        u6 = Conv2DTranspose(512, (3, 3), strides=(2, 2), padding='same', kernel_initializer='he_normal')(c5)
        u6 = concatenate([u6, c4], axis=3)
        c6 = Conv2D(512, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(u6)
        # c6 = Dropout(0.2)(c6)
        c6 = Conv2D(512, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c6)

        u7 = Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same', kernel_initializer='he_normal')(c6)
        u7 = concatenate([u7, c3], axis=3)
        c7 = Conv2D(256, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(u7)
        c7 = Dropout(0.2)(c7)
        c7 = Conv2D(256, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c7)

        u8 = Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same', kernel_initializer='he_normal')(c7)
        u8 = concatenate([u8, c2], axis=3)
        c8 = Conv2D(128, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(u8)
        c8 = Dropout(0.1)(c8)
        c8 = Conv2D(128, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c8)

        u9 = Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same', kernel_initializer='he_normal')(c8)
        u9 = concatenate([u9, c1], axis=3)
        c9 = Conv2D(64, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(u9)
        c9 = Conv2D(64, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c9)
        c9 = Conv2D(32, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c9)

        outputs = Conv2D(1, (1, 1), activation='sigmoid')(c9)

        model = Model(inputs=[inputs], outputs=[outputs])

        optimizer = Adam(learning_rate=1e-5)

        model.compile(optimizer=optimizer,
                      loss=[dice_coef_loss],
                      metrics=[dice_coef],
                      )
    model.summary()

    return model


def build_model2():
    strategy = tf.distribute.MirroredStrategy()
    with strategy.scope():
        inputs = Input((param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS))

        c1 = Conv2D(16, (3, 3), activation='relu', padding='same')(inputs)
        c1 = Conv2D(32, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c1)
        p1 = MaxPooling2D((2, 2))(c1)

        c2 = Conv2D(64, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(p1)
        c2 = Conv2D(64, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c2)
        p2 = MaxPooling2D((2, 2))(c2)

        c3 = Conv2D(128, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(p2)
        c3 = Conv2D(128, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c3)
        p3 = MaxPooling2D((2, 2))(c3)

        c4 = Conv2D(256, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(p3)
        c4 = Dropout(0.5)(c4)
        c4 = Conv2D(256, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c4)
        p4 = MaxPooling2D(pool_size=(2, 2))(c4)

        c5 = Conv2D(512, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(p4)
        c5 = Conv2D(512, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c5)
        c5 = Dropout(0.5)(c5)

        u6 = Conv2DTranspose(256, (3, 3), strides=(2, 2), padding='same', kernel_initializer='he_normal')(c5)
        u6 = concatenate([u6, c4], axis=3)
        c6 = Conv2D(256, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(u6)
        c6 = Conv2D(256, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c6)

        u7 = Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same', kernel_initializer='he_normal')(c6)
        u7 = concatenate([u7, c3], axis=3)
        c7 = Conv2D(128, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(u7)
        c7 = Dropout(0.2)(c7)
        c7 = Conv2D(128, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c7)

        u8 = Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same', kernel_initializer='he_normal')(c7)
        u8 = concatenate([u8, c2], axis=3)
        c8 = Conv2D(64, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(u8)
        c8 = Dropout(0.1)(c8)
        c8 = Conv2D(64, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c8)

        u9 = Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same', kernel_initializer='he_normal')(c8)
        u9 = concatenate([u9, c1], axis=3)
        c9 = Conv2D(32, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(u9)
        c9 = Conv2D(32, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c9)
        c9 = Conv2D(16, (3, 3), activation='relu', padding='same', kernel_initializer='he_normal')(c9)

        outputs = Conv2D(1, (1, 1), activation='sigmoid')(c9)

        model = Model(inputs=[inputs], outputs=[outputs])

        optimizer = Adam(learning_rate=1e-6)

        model.compile(optimizer=optimizer,
                      loss=[dice_coef_loss],
                      metrics=[dice_coef],
                      )
    model.summary()

    return model


def build_model_zf_unet():
    strategy = tf.distribute.MirroredStrategy()
    with strategy.scope():
        inputs = Input((param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS))

        conv1 = Conv2D(32, (3, 3), padding='same')(inputs)
        conv1 = BatchNormalization()(conv1)
        conv1 = Activation('relu')(conv1)
        conv1 = Conv2D(32, (3, 3), padding='same')(conv1)
        conv1 = BatchNormalization()(conv1)
        conv1 = Activation('relu')(conv1)
        pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

        conv2 = Conv2D(64, (3, 3), padding='same')(pool1)
        conv2 = BatchNormalization()(conv2)
        conv2 = Activation('relu')(conv2)
        conv2 = Conv2D(64, (3, 3), padding='same')(conv2)
        conv2 = BatchNormalization()(conv2)
        conv2 = Activation('relu')(conv2)
        pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

        conv3 = Conv2D(128, (3, 3), padding='same')(pool2)
        conv3 = BatchNormalization()(conv3)
        conv3 = Activation('relu')(conv3)
        conv3 = Conv2D(128, (3, 3), padding='same')(conv3)
        conv3 = BatchNormalization()(conv3)
        conv3 = Activation('relu')(conv3)
        pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

        conv4 = Conv2D(256, (3, 3), padding='same')(pool3)
        conv4 = BatchNormalization()(conv4)
        conv4 = Activation('relu')(conv4)
        conv4 = Conv2D(256, (3, 3), padding='same')(conv4)
        conv4 = BatchNormalization()(conv4)
        conv4 = Activation('relu')(conv4)
        pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

        conv5 = Conv2D(512, (3, 3), padding='same')(pool4)
        conv5 = BatchNormalization()(conv5)
        conv5 = Activation('relu')(conv5)
        conv5 = Conv2D(512, (3, 3), padding='same')(conv5)
        conv5 = BatchNormalization()(conv5)
        conv5 = Activation('relu')(conv5)
        pool5 = MaxPooling2D(pool_size=(2, 2))(conv5)

        conv6 = Conv2D(1024, (3, 3), padding='same')(pool5)
        conv6 = BatchNormalization()(conv6)
        conv6 = Activation('relu')(conv6)
        conv6 = Conv2D(1024, (3, 3), padding='same')(conv6)
        conv6 = BatchNormalization()(conv6)
        conv6 = Activation('relu')(conv6)

        up7 = UpSampling2D(size=(2, 2))(conv6)
        up7 = concatenate([up7, conv5], axis=3)
        conv8 = Conv2D(512, (3, 3), padding='same')(up7)
        conv8 = BatchNormalization()(conv8)
        conv8 = Activation('relu')(conv8)
        conv8 = Conv2D(512, (3, 3), padding='same')(conv8)
        conv8 = BatchNormalization()(conv8)
        conv8 = Activation('relu')(conv8)

        up9 = UpSampling2D(size=(2, 2))(conv8)
        up9 = concatenate([up9, conv4], axis=3)
        conv10 = Conv2D(256, (3, 3), padding='same')(up9)
        conv10 = BatchNormalization()(conv10)
        conv10 = Activation('relu')(conv10)
        conv10 = Conv2D(256, (3, 3), padding='same')(conv10)
        conv10 = BatchNormalization()(conv10)
        conv10 = Activation('relu')(conv10)

        up11 = UpSampling2D(size=(2, 2))(conv10)
        up11 = concatenate([up11, conv3], axis=3)
        conv12 = Conv2D(128, (3, 3), padding='same')(up11)
        conv12 = BatchNormalization()(conv12)
        conv12 = Activation('relu')(conv12)
        conv12 = Conv2D(128, (3, 3), padding='same')(conv12)
        conv12 = BatchNormalization()(conv12)
        conv12 = Activation('relu')(conv12)

        up13 = UpSampling2D(size=(2, 2))(conv12)
        up13 = concatenate([up13, conv2], axis=3)
        conv14 = Conv2D(64, (3, 3), padding='same')(up13)
        conv14 = BatchNormalization()(conv14)
        conv14 = Activation('relu')(conv14)
        conv14 = Conv2D(64, (3, 3), padding='same')(conv14)
        conv14 = BatchNormalization()(conv14)
        conv14 = Activation('relu')(conv14)

        up15 = UpSampling2D(size=(2, 2))(conv14)
        up15 = concatenate([up15, conv1], axis=3)
        conv16 = Conv2D(32, (3, 3), padding='same')(up15)
        conv16 = BatchNormalization()(conv16)
        conv16 = Activation('relu')(conv16)
        conv16 = Conv2D(32, (3, 3), padding='same')(conv16)
        conv16 = BatchNormalization()(conv16)
        conv16 = Activation('relu')(conv16)
        conv16 = Dropout(0.2)(conv16)

        conv_final = Conv2D(1, (1, 1), activation='sigmoid')(conv16)

        model = Model(inputs=[inputs], outputs=[conv_final])

        model.load_weights(os.path.join(param.ROOT_PATH, "data", "models", "zf_unet.h5"))

        optimizer = Adam()
        model.compile(optimizer=optimizer, loss=[dice_coef_loss], metrics=[dice_coef])

    model.summary()

    return model


def train_model(model, x_train, y_train, epochs=1):
    checkpointer = ModelCheckpoint(
        os.path.join(param.ROOT_PATH, 'data', 'models', 'saved_models', 'model-wounds-build2.h5'), verbose=1,
        save_best_only=True)
    wandb_cb = WandbCallback()
    results = model.fit(x_train, y_train,
                        batch_size=8,
                        validation_split=param.val_split,
                        callbacks=[
                            checkpointer,
                            wandb_cb,
                        ],
                        epochs=epochs,
                        )
    return results


def train_model_generator(model, train_generator, val_generator, save_name):
    checkpointer = ModelCheckpoint(os.path.join(param.ROOT_PATH, 'data', 'models', 'saved_models', save_name),
                                   verbose=1,
                                   save_best_only=True,
                                   )
    wandb_cb = WandbCallback()

    results = model.fit(train_generator,
                        batch_size=param.BATCH_SIZE,
                        validation_data=val_generator,
                        validation_batch_size=param.BATCH_SIZE,
                        callbacks=[
                                   checkpointer,
                                   wandb_cb,
                                   ],
                        epochs=param.EPOCHS,
                        )

    return results


def load_model_(path):
    return load_model(path, custom_objects={'dice_coef_loss': dice_coef_loss, 'dice_coef': dice_coef})


def continue_training(model, train_generator, val_generator, l_r):
    K.set_value(model.optimizer.lr, l_r)

    checkpointer = ModelCheckpoint(
        os.path.join(param.ROOT_PATH, 'data', 'models', 'saved_models', 'model-wounds-zf_unet-continued.h5'),
        verbose=1,
        save_best_only=True,
    )
    wandb_cb = WandbCallback()
    results = model.fit(train_generator,
                        batch_size=param.BATCH_SIZE,
                        validation_data=val_generator,
                        validation_batch_size=param.BATCH_SIZE,
                        callbacks=[
                            checkpointer,
                            wandb_cb,
                        ],
                        epochs=param.EPOCHS,
                        )

    return results


def compile_model(model):
    opt = Adam(learning_rate=1e-5)
    model.compile(optimizer=opt, loss=[dice_coef_loss], metrics=[dice_coef])
    model.summary()

    return model
