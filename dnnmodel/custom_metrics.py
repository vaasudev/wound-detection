from tensorflow.keras import backend as K


def custom_mean_iou(y_true, y_pred):
    y_pred = K.cast(K.greater(y_pred, 0.5), dtype='float32')  # .5 is the threshold
    y_true = K.cast(K.greater(y_true, 0.5), dtype='float32')  # .5 is the threshold

    inter = K.sum(K.sum(K.squeeze(y_true * y_pred, axis=3), axis=2), axis=1)
    union = K.sum(K.sum(K.squeeze(y_true + y_pred, axis=3), axis=2), axis=1)

    return K.mean((inter + K.epsilon()) / (union - inter + K.epsilon()))


def dice_coef(y_true, y_pred):
    y_true_ = K.flatten(y_true)
    y_pred_ = K.flatten(y_pred)

    intersection = K.sum(y_true_ * y_pred_)
    union = K.sum(y_true_) + K.sum(y_pred_)  # - intersection
    return K.mean(2 * (intersection + 1.0) / (union + 1.0))