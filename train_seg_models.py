import tensorflow.compat.v1 as tf
print(tf.__version__)
tf.disable_eager_execution()
# tf.compat.v1.disable_v2_behavior()
print(tf.test.is_gpu_available())

import os

os.environ['SM_FRAMEWORK'] = 'tf.keras'

import segmentation_models as sm
import datamanipulation.preprocess as preprocess
import parameters as param
from tensorflow.keras.callbacks import ModelCheckpoint
import wandb
from wandb.keras import WandbCallback

wandb.init(project="wound-segmentation")

BACKBONE = 'vgg16'

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)

train_images_generator, val_images_generator = preprocess.get_train_generator()

model = sm.Unet(BACKBONE, classes=1, activation='sigmoid', input_shape=(param.IMG_WIDTH, param.IMG_HEIGHT, param.IMG_CHANNELS))
model.compile(
    'Adam',
    loss=sm.losses.bce_jaccard_loss,
    metrics=[sm.metrics.iou_score],)
model.summary()

checkpointer = ModelCheckpoint(os.path.join(param.ROOT_PATH, 'data', 'models', 'model-wounds-resnet34.h5'),
                               verbose=1,
                               save_best_only=True,
                               )
wandb_cb = WandbCallback()
# results = model.fit_generator(train_images_generator,
#                               epochs=param.EPOCHS,
#                               steps_per_epoch=train_images_generator.__len__(),
#                               verbose=1,
#                               # callbacks=[wandb_cb, checkpointer],
#                               validation_data=val_images_generator,
#                               validation_steps=val_images_generator.__len__(),
#                               )

results = model.fit(train_images_generator,
                    # batch_size=param.BATCH_SIZE,
                    validation_data=val_images_generator,
                    # validation_batch_size=param.BATCH_SIZE,
                    callbacks=[  # earlystopper,
                        checkpointer,
                        # tensorboard,
                        wandb_cb,
                    ],
                    epochs=param.EPOCHS,
                    )
